<?php
//v1.0
/**
 * DB
 *   Handles basic Database functions
 *   DB tables for this class are prefixed by db_
 */
class DBConfig {
	const HOSTNAME = 'localhost';
	const USERNAME = 'root';
	const PASSWORD = 'root';
	const SCHEMA = 'pushprice';
}

class DB {
	
	static $TABLES_PREFIX = 'db';
	
	static $mysqli;
	
	static function set_mysqli() {
		DB::$mysqli = mysqli_connect(DBConfig::HOSTNAME, DBConfig::USERNAME, DBConfig::PASSWORD, DBConfig::SCHEMA);
	}
	
	static function query($qs) {
		if(!DB::$mysqli) DB::set_mysqli();
			
		if($res = mysqli_query(DB::$mysqli,$qs)) {
			$resultset = array();
			while($row = mysqli_fetch_assoc($res)) {
				$resultset[] = $row;
			}
			mysqli_free_result($res);
			return $resultset;
		} else {
			return array();
		}
	}
	
	static function update($qs) {
		if(!DB::$mysqli) DB::set_mysqli();
		
		if(mysqli_query(DB::$mysqli,$qs)) {
			return mysqli_affected_rows(DB::$mysqli);
		} else {
			return 0;
		}
	}
	
	static function select($qs) { 
		return DB::query($qs);
	}
	
	static function delete($qs) {
		 return DB::update($qs); 
	}
	
	static function insert($qs) {
		 return DB::update($qs); 
	}
	
	static function escape($s) {
		if(!DB::$mysqli) DB::set_mysqli();
		return mysqli_real_escape_string(DB::$mysqli,$s);
	}
	static function insert_id() {
		if(!DB::$mysqli) DB::set_mysqli();
		return mysqli_insert_id(DB::$mysqli);
	}
}
?>