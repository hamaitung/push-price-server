<?php

require_once('SimplePush/Manager.php');

define("PEM_FILE", "SimplePush/pushprice.pem");

$sharedManager = Manager::sharedManager();

// $sharedManager->checkPriceCondition("AAPL",true,500);
// $sharedManager->checkPriceCondition("AAPL",false,500);

// retrieve all alert from database

$count = 0;
while (true) {
	$alertArray = $sharedManager->getAlertList();
	foreach ($alertArray as $alert) {
		$symbol = $alert['symbol'];
		$lowest = $alert['lowest'];
		$highest = $alert['highest'];
		if (($currentPrice = $sharedManager->checkPriceCondition($symbol, $lowest, $highest)) > 0){
			$token = $alert['token'];
			$alertID = $alert['id'];
			$message = $symbol . " current price: " . $currentPrice;
			echo " --- Push notif: " . $token . " message: " . $message;
			if ($sharedManager->pushNotification($token, "01655525236", $message, PEM_FILE)){
				echo " --- Push notif: " . $token . " message: " . $message;
				file_put_contents("checkalert_output.txt", date("F j, Y, g:i a")." -- Push notif: " . $token . " message: " . $message . "\n", FILE_APPEND);
				$sharedManager->deleteAlert($alertID);
			}
		}
	}
	// file_put_contents("checkalert_output.txt", "$count\n");
	if (++$count > 28) break;
	sleep(30);
}