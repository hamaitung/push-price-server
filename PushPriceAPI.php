<?php

/*
Cron command

	php /home/ywcheong/public_html/livedevs.com/pushprice/CheckAlert.php
	php /home/ywcheong/public_html/livedevs.com/pushprice/RetrievePrice.php

Terminal command to run on loop process

	nohup php RetrievePrice.php > retrieve_output.txt 2> retrieve_err.txt < /dev/null &	
	nohup php CheckAlert.php > checkalert_output.txt 2> checkalert_err.txt < /dev/null &
	
	kill running process: kill -15 8976
	List all process:	 ps a
	php source: which php

	ssh link: ssh -p 2222 ywcheong@nutshel.com
*/


require_once('SimplePush/Manager.php');

$sharedManager = Manager::sharedManager();

$action = $_GET['action'];
if ($action == "retrieveAlertList"){
	$token = $_GET['token'];

	header('Content-Type: application/json');
	echo($sharedManager->getAlertListForDevice($token));
} 
elseif ($action == "registerAlert"){
	$token = $_GET['token'];
	$symbol = $_GET['symbol'];
	$isEnabled = $_GET['isEnabled'];
	$lowest = $_GET['lowest'];
	$highest = $_GET['highest'];
	echo ($sharedManager->insertToUserTable($token, $symbol, $lowest, $highest, $isEnabled));
} elseif ($action == "toggleAlert") {
	#enable/disable the alert
	$alertID = $_GET['id'];
	$isEnabled = $_GET['isEnabled'];
	echo ($sharedManager->updateAlert($alertID, $isEnabled));
}
elseif ($action == "deleteAlert"){
	$alertID = $_GET['id'];

	if ($sharedManager->deleteAlert($alertID)) echo "1";
	else echo "0";
}
elseif ($action == "updateAlert"){
	$alertID = $_GET['id'];
	$isEnabled = $_GET['isEnabled'];
	$lowest = $_GET['lowest'];
	$highest = $_GET['highest'];
	echo ($sharedManager->updateFullAlert($alertID, $lowest, $highest, $isEnabled));
}

?>