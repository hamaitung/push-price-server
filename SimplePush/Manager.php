<?php

/*

Check Alert cron path:
/usr/local/bin/php/home/ywcheong/public_html/livedevs.com/pushprice/CheckAlert.php

*/


// define(HOSTNAME, "localhost", true);
// define(USERNAME, "ywcheong_admin", true);
// define(PASSWORD, "01655525236", true);
// define(SCHEMA, "ywcheong_pushprice", true);
require_once('Config.php');

/* Singleton class that manages all actions of server */
class Manager {
	private static $instance;
	private $connection;
	
	private function __construct() {}

    public static function sharedManager() {
        if (!isset(self::$instance)) {
            $className = __CLASS__;
            self::$instance = new $className;
        }
        return self::$instance;
    }

	/* Manage database */
	private function initializeConnection() {
		$this->connection = mysqli_connect(HOSTNAME, USERNAME, PASSWORD, SCHEMA);
	}
	
	private function query($queryString) {
		if (!isset($this->connection))
			$this->initializeConnection();
			
		$resultSet = array();	
		if ($result = mysqli_query($this->connection, $queryString)) {
			while ($row = mysqli_fetch_assoc($result)) {
				$resultSet[] = $row;
			}
			
			mysqli_free_result($result);
		}
		
		return $resultSet;
	}

	private function update($queryString) {
		if (!isset($this->connection))
			$this->initializeConnection();
		
		if (mysqli_query($this->connection, $queryString)) {
			return mysqli_affected_rows($this->connection);
		} else {
			return 0;
		}
	}
	
	private function select($queryString) { 
		return $this->query($queryString);
	}
	
	private function delete($queryString) {
		return $this->update($queryString);
	}
	
	private function insert($queryString) {
		return $this->update($queryString);
	}

	public function getHtml($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$content = curl_exec($ch);
		curl_close($ch);
		
		return $content;
	}

	/* Database Functions */

	public function insertToUserTable($token, $symbol, $lowest, $highest, $isEnabled) {
		$queryString = "INSERT INTO alerts(symbol, lowest, highest, isEnabled, token) VALUES  ('$symbol', $lowest, $highest, $isEnabled, '$token')";

		if (!isset($this->connection))
			$this->initializeConnection();


		if (mysqli_query($this->connection, $queryString)) {
			if (mysqli_affected_rows($this->connection)){
				$id = mysqli_insert_id($this->connection);
				return $id;
			} 
			return 0;
		} else {
			return 0;
		}

		return 0;
	}

	public function updateAlert($alertID, $isEnabled){		
		if (!isset($this->connection))
			$this->initializeConnection();
		if(mysqli_connect_errno()){
			echo "Connection error ", mysqli_connect_error();
			return false;
		}
		$sql = "UPDATE alerts SET isEnabled=$isEnabled WHERE id=$alertID";
		if(mysqli_query($this->connection, $sql)){
			return true;
		}
		return false;
	}

	public function updateFullAlert($alertID, $lowest, $highest, $isEnabled) {
		$queryString = "UPDATE alerts SET lowest=$lowest, highest=$highest, isEnabled=$isEnabled WHERE id=$alertID";

		if (!isset($this->connection))
			$this->initializeConnection();


		if(mysqli_query($this->connection, $queryString)){
			return 1;
		}
		return 0;
	}

	public function getAlertList() {
		$queryString = "SELECT * FROM alerts WHERE isEnabled=1 GROUP BY symbol, token";
		return $this->select($queryString);
	}

	public function deleteAlert($alertID) {
		$queryString = "DELETE FROM alerts WHERE id = $alertID";
		return $this->delete($queryString);
	}

	public function getUniqueSymbolList(){
		$start = intval(file_get_contents("symbols.txt"));
		$end = $start + 10;
		
		$queryString = "SELECT DISTINCT(`symbol`) FROM alerts LIMIT $start, $end";

		file_put_contents("symbols.txt", $end);

		return $this->select($queryString);
	}

	public function getAlertListForDevice($token){
		$queryString = "SELECT * FROM alerts WHERE token = '$token' GROUP BY symbol";			
		$records = $this->select($queryString);

		return json_encode($records);
	}

	/* Save / Write JSON to file */
	public function writeJSON($data, $path) {
		file_put_contents($path, json_encode($data));
	}

	public function readJSON($path) {
		if (file_exists($path)) {
			if ($content = file_get_contents($path))
				return json_decode($content, true);
		}

		return false;
	}
	
	/* Update Price and save to static file*/
	public function retrievePriceForSymbols($symbols) {
		// prepare the SymbolsString for the requestURL
		foreach ($symbols as $value) {
			$symbolsString = $symbolsString . $value['symbol'] . ',';
		}

		$requestUrl = "http://finance.ipad.mobile.yahoo.com/2e76/v5/quote/symbol/$symbolsString?view=detail&format=json";
		//echo "Request URL = " . $requestUrl ;

		$data = json_decode($this->getHtml($requestUrl), true);

		foreach ($data['finance']['quotes']['quote'] as $values) {
			$price = $values['price']['raw'];
			$realPrice = $values['realtime_price']['raw'];
			$symbol = $values['symbol'];

			// create price array 
			$priceArray = array("price" => $price, "realtime_price" => $realPrice);
			// static filePath
			$filePath = "data/" . $symbol . ".json";
			//echo ($filePath);
			// save to static file
			$this->writeJSON($priceArray, $filePath);
		}
	}

	/* Check price for Symbol with condition */
	public function checkPriceCondition($symbol, $lowest, $highest){
		// Read JSON file to get price array
		$filePath = "data/" . $symbol . ".json";
		if ($priceArray = $this->readJSON($filePath)) {
				/*
				Get price Value
				--> if realPrice not NULL use it
				Else use the price
			*/ 
			if(isset($priceArray['realtime_price'])){
				$price = $priceArray['realtime_price'];
			} else {
				$price = $priceArray['price'];
			}

			//testing
			// return $price;

			if($price < $lowest || $price > $highest){ //out of range
				return $price;
			}
			return -1;
		} else {
			echo $symbol . " : NO JSON file existed<br>";
			return -1;
		}
	}


	/* Manage responses for clients */
	public function responseToClients($state, $response = array()) {
		echo json_encode(array(
			'notify' => $state,
			'result' => $response,
		));
	}

	/* Push notification */
	public function pushNotification($deviceToken, $passphrase, $message, $pemFile) {
		$isSandbox = true;
		$deviceToken = str_replace(' ', '', $deviceToken);
		$url = $isSandbox ? 'ssl://gateway.sandbox.push.apple.com:2195' : 'ssl://gateway.push.apple.com:2195';

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
		// $this->responseToClients(NOTIFY_PUSH_FAILED, "Failed to connect: $err $errstr")
		if (!$fp)
			return false;

		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
		);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		// Close the connection to the server
		fclose($fp);

		//$this->responseToClients(NOTIFY_PUSH_FAILED, "Fail to push notification")
		if (!$result)
			return false;
		
		return true;
	}
}

?>